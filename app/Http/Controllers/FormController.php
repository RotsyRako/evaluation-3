<?php

namespace App\Http\Controllers;

use App\Models\DashboardBenefice;
use App\Models\DashboardDepense;
use App\Models\DashboardRecette;
use App\Models\Depense;
use App\Models\DepenseType;
use App\Models\Facture;
use App\Models\FactureDetailActe;
use App\Models\FactureFullDetail;
use App\Models\FacturePatient;
use App\Models\Genre;
use App\Models\Patient;
use App\Models\PatientGenre;
use App\Models\TypeActe;
use App\Models\TypeDepense;
use App\Models\Utilisateur;
use App\Models\UtilisateurType;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class FormController extends Controller
{
    // AUTHENTIFICATION DE L'UTILISATEUR
    public function authentification(Request $request)
    {
        $attributes = [
            'login' => $request->identifiant,
            'mdp' => $request->motDePasse
        ];
        try {
            $utilisateur = new UtilisateurType($attributes);
            $utilisateur = $utilisateur->getUtilisateur();
            if($utilisateur!=null){
                if($utilisateur->idTypeUtilisateur==1){
                    //Administrateur
                    return redirect('nouveautableaubord');
                } else if($utilisateur->idTypeUtilisateur==2){
                    //Utilisateur
                    $utilisateur = Utilisateur::findOrFail($utilisateur->id);
                    if(null!==(Session::get('utilisateur'))){
                        Session::forget('utilisateur');
                    }
                    Session::put('utilisateur',$utilisateur);
                    return redirect('saisiefacture');
                }
            } else {
                //non identifié
                return view('identify')
                ->with('error',true);
            }
        } catch (\Throwable $th) {
            error_log($th);
        }
    }

    // DECONNEXION
    public function deconnexion(){
        Session::flush();
        Session::regenerateToken();

        return view('identify');
    }
    
    // CHARGEMENT DES PAGES DE LISTE
    public function gestionpatient(){
        $patient = new PatientGenre();
        $genre = new Genre();
        return view('listepatients')
        ->with('genre',$genre::all())
        ->with('allpatients',$patient::all());
    }
    
    public function gestiontypeacte(Request $request){
        $erreur = $request->query('erreur');
        $acte = new TypeActe();
            
        if($erreur!=null){
            return view('listeactes')
            ->with('erreur',$erreur)
            ->with('touslesactes',$acte::all());
        }
        return view('listeactes')
            ->with('touslesactes',$acte::all());
    }

    public function gestiontypedepense(){
        $dep = new TypeDepense();
            
        return view('listedepenses')
        ->with('toutesdepenses',$dep::all());
    }

    public function listefactures(){
        $facture = new FacturePatient();
        return view('listefactures')
        ->with('allfact',$facture::all());
    }

    public function listedepenses(){
        $depense = new DepenseType();
        return view('listedepensesfait')
        ->with('depense',$depense::all());
    }

    // FONCTIONS D'AJOUT
    public function ajouterpatient(Request $request){
        $today = Carbon::today();
        $anotherDate = Carbon::createFromFormat('Y-m-d', $request->naissance);

        $attributes = [
            'nom' => $request->nom,
            'dateNaissance' => $request->naissance,
            'idGenre' => $request->genre,
            'remboursement' => $request->remboursement
        ];
        if($anotherDate->greaterThan($today)){
            $patient = new PatientGenre();
            $genre = new Genre();
            return view('listepatients')
            ->with('genre',$genre::all())
            ->with('error','error')
            ->with('allpatients',$patient::all());
        }
        $patient = new Patient($attributes);
        $patient->save();

        return redirect('gestionpatient');
    }

    public function ajoutertypeacte(Request $request){
        $validator = Validator::make($request->all(), [
            'code' => 'required|unique:TypeActe',
            'typeacte' => 'required',
            'budgetannuel' => 'required'
        ]);
    
        if ($validator->fails()) {
            return redirect()->route('gestiontypeacte', ['erreur' => 'code']);
        }
    
        $code = strtoupper(substr($request->code, 0, 3));;
        $attributes = [
            'typeActe' => $request->typeacte,
            'budgetAnnuel' => $request->budgetannuel,
            'code' => $code
        ];
        $typeacte = new TypeActe($attributes);
        $typeacte->save();
    
        return redirect('gestiontypeacte');
    }

    public function ajoutertypedepense(Request $request){
        $validator = Validator::make($request->all(), [
            'code' => 'required|unique:TypeDepense',
            'depense' => 'required',
            'budgetannuel' => 'required'
        ]);
    
        if ($validator->fails()) {
            return redirect()->route('gestiontypedepense', ['erreur' => 'code']);
        }
    
        $code = strtoupper(substr($request->code, 0, 3));
        $attributes = [
            'typeDepense' => $request->depense,
            'budgetAnnuel' => $request->budgetannuel,
            'code' => $code
        ];
        $typeacte = new TypeDepense($attributes);
        $typeacte->save();
    
        return redirect('gestiontypedepense');
    }

    public function saveInvoice(Request $request)
    {
        $patientId = $request->input('patient');
        $dateFacture = $request->input('datefacture');
        $value = session('utilisateur');
        $idUtilisateur = $value->id;

        $attributes=[
            'idPatient'=>$patientId,
            'idUtilisateur' =>$idUtilisateur,
            'dateFacture' =>$dateFacture
        ];

        $newFacture = new Facture($attributes);
        $newFacture->save();

        $typeActes = $request->input('typeActe');
        $couts = $request->input('cout');
        foreach ($typeActes as $index => $typeActeId) {
            DB::table('FactureDetail')->insert([
                'idFacture' => $newFacture->id,
                'idTypeActe' => $typeActeId,
                'prix' => $couts[$index],
            ]);
        }

        //VERIFICATION SI PATIENT REMBOURSE
        $patient = Patient::findOrFail($patientId);
        if($patient->remboursement==true){
            $sum = FactureFullDetail::where('id', $newFacture->id)
            ->sum('prix');

            $valeurremboursement = $sum*0.2;
            DB::table('FactureDetail')->insert([
                'idFacture' => $newFacture->id,
                'idTypeActe' => 5,
                'prix' => $valeurremboursement
            ]);
        }
        
        return redirect('saisiefacture');
    }

    public function savedepense(Request $request){
        $value = session('utilisateur');
        $idUtilisateur = $value->id;
        $depID = $request->input('typedep');
        $prix = $request->input('cout');
        $dateD = $request->input('datedep');

        $attributes=[
            'idUtilisateur' =>$idUtilisateur,
            'idTypeDepense' =>$depID,
            'dateDepense' =>$dateD,
            'cout' =>$prix
        ];

        $newDep = new Depense($attributes);
        $newDep->save();

        return redirect('saisiedepense');
    }

    // AJOUT DES DEPENSES MULTIPLES
    public function multidepense(Request $request){
        $erreur = 0;
        $erreurdate = 0;
        $erreurcode = [];
        $erreurmontant = [];

        $value = session('utilisateur');
        $idUtilisateur = $value->id;
        $depID = 0;
        $depcode = $request->input('typedep');
        $prix = $request->input('montant');
        $jour = $request->input('jour');
        $annee = $request->input('annee');
        
        $selectedMonths = $request->input('months');
        
        foreach ($selectedMonths as $monthValue) {
            $date = $annee . '-' . str_pad($monthValue, 2, '0', STR_PAD_LEFT) . '-' . str_pad($jour, 2, '0', STR_PAD_LEFT);

            //VERIFICATION DATE VALIDE
            if($this->isDateValid($date)!=true){
                $erreur = $erreur+1;
                $erreurdate = 1;
                //return redirect()->back()->with('erreur',3);
            }

            //VERIFICATION CODE VALIDE
            $typeDepense  = TypeDepense::where('code','=',$depcode)->get()->first();
            
            if($typeDepense==null){                
                $erreur = $erreur+1;
                $erreurcode[]=$depcode;
            }
//            if($this->getIDDepense($depcode)==0||$this->getIDDepense($depcode)!==-1){
//            }

            //VERIFICATION NOMBRE VALIDE
            if(is_numeric($prix)==false||intval($prix)<0){
                $erreur = $erreur+1;
                $erreurmontant []=$prix; 
            }
        }
        
        if($erreur==0){
        foreach ($selectedMonths as $monthValue) {
            $date = $annee . '-' . str_pad($monthValue, 2, '0', STR_PAD_LEFT) . '-' . str_pad($jour, 2, '0', STR_PAD_LEFT);
            $depID = $this->getIDDepense($depcode);
            Depense::create([
                'idUtilisateur' => $idUtilisateur,
                'idTypeDepense' => $depID,
                'dateDepense' => $date,
                'cout' => $prix
            ]);
        }
            return redirect('saisiedepense');
        } else {
            return redirect()->back()
            ->with('erreurdate',$erreurdate)
            ->with('erreurcode',$erreurcode)
            ->with('erreurmontant',$erreurmontant);
        }
    }    

    //FONCTIONS DE MODIFICATION
    public function modifierpatient(Request $request){
        $id = $request->id;
        $today = Carbon::today();
        $anotherDate = Carbon::createFromFormat('Y-m-d', $request->naissance);

        $patient = Patient::findOrFail($id);
        $patient->nom = $request->nom;
        if($anotherDate->greaterThan($today)){
            $patient = new PatientGenre();
            $genre = new Genre();
            return view('listepatients')
            ->with('genre',$genre::all())
            ->with('error','error')
            ->with('allpatients',$patient::all());
        }
        $patient->dateNaissance = $request->naissance;
        $patient->idGenre = $request->genre;
        $patient->remboursement = $request->remboursement;
        $patient->update();

        return redirect('gestionpatient');
    }

    public function modifiertypeacte(Request $request){
        $id = $request->id;

        $typeacte = TypeActe::findOrFail($id);
        $typeacte->typeActe = $request->input('typeacte');
        $typeacte->code = $request->input('code');
        $typeacte->budgetAnnuel = $request->input('budgetannuel');
        $typeacte->update();

        return redirect('gestiontypeacte');
    }

    public function modifiertypedepense(Request $request){
        $id = $request->id;

        $typedep = TypeDepense::findOrFail($id);
        $typedep->typeDepense = $request->typedep;
        $typedep->code = $request->input('code');
        $typedep->budgetAnnuel = $request->input('budgetannuel');
        $typedep->update();

        return redirect('gestiontypedepense');
    }

    // FONCTIONS DE SUPPRESSION
    public function supprimerpatient(Request $request, $id){
        $id = $request->id;

        $patient = Patient::findOrFail($id);
        $patient->etat = 2;
        $patient->update();

        return redirect('gestionpatient');
    }
    
    public function supprimertypeacte(Request $request, $id){
        $id = $request->id;

        $typeacte = TypeActe::findOrFail($id);
        $typeacte->etat = 2;
        $typeacte->update();

        return redirect('gestiontypeacte');
    }
 
    public function supprimertypedepense(Request $request, $id){
        $id = $request->id;

        $typedep = TypeDepense::findOrFail($id);
        $typedep->etat = 2;
        $typedep->update();

        return redirect('gestiontypedepense');
    }

    //PREMIER CHARGEMENT DU NOUVEAU DE BORD
    public function nouveautableaubord(){
        $monthNames = [
            'Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin',
            'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'
        ];
        return view('nouveautableaubord',compact('monthNames'))
        ->with('condition','');
    }
 
    //CHARGEMENT DES REQUETES VENANT DU FILTRE DANS LE NOUVEAU TABLEAU DE BORD
    public function nouveauloadMonthData(Request $request){
        $monthNames = [
            'Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin',
            'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'
        ];
        
        $month = $request->input('mois');
        $year = $request->input('annee');
        
        //RECETTES
        $typeacte = TypeActe::all();
        $donneesrecettes =[];
        foreach($typeacte as $act){
            $dashrecette = new DashboardRecette();
            $dashrecette->initDashboardData($act->id,$month,$year);
            $dashrecette->formatData();
            $donneesrecettes[$act->id]=$dashrecette;
        }

        //DEPENSES
        $typedep = TypeDepense::all();
        $donneesdepenses = [];
        foreach($typedep as $dep){
            $dashdepense = new DashboardDepense();
            $dashdepense->initDashboardData($dep->id,$month,$year);
            $dashdepense->formatData();
            $donneesdepenses[$dep->id]=$dashdepense;
        }

        //BENEFICES
        $donneesbenef = new DashboardBenefice();
        $donneesbenef->initDashboardData($month,$year);
        $donneesbenef->formatData();

        return view('nouveautableaubord',compact('monthNames'))
        ->with('donneesdepenses', $donneesdepenses)
        ->with('donneesrecettes', $donneesrecettes)
        ->with('donneesbenef', $donneesbenef)
        ->with('condition', 1)
        ->with('typeacte', $typeacte)
        ->with('typedep', $typedep);
    }
    
    //PAGE DE SAISIE
    public function saisiefacture(){
        $user = new Patient();
        $act = new TypeActe();
        return view('saisiefacture')
        ->with('types',$act::all())
        ->with('allusers',$user::all());
    }

    public function saisiedepense(){
        $monthNames = [
            'Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin',
            'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'
        ];
        $monthValues = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

        $dep = new TypeDepense();
        return view('saisiedepense')
        ->with('dep',$dep::all())
        ->with('monthNames',$monthNames)
        ->with('monthValues',$monthValues);
    }

    // CHARGEMENT DES DETAILS DE FACTURE
    public function detailfacture(Request $request, $id){
        $facture = FacturePatient::find($id);
        $details  = FactureDetailActe::where('idFacture','=',$id)->get();
        $datefacture = $facture->getDateFactureFormatted();
        $datenaissance =  $facture->getDateNaissanceFormatted();
        $total = $facture->getTotalAmountAttribute();
        return view('detailfacture')
        ->with('facture',$facture)
        ->with('datefacture',$datefacture)
        ->with('datenaissance',$datenaissance)
        ->with('total',$total)
        ->with('details',$details);
    }
   
    // IMPORT CSV
    public function importcsv(Request $request)
    {
        $file = $request->file('csv_file');
        $filePath = $file->path();
        
        $contents = file_get_contents($filePath);
        $rows = explode(PHP_EOL, $contents);
        
        foreach ($rows as $row) {
            $data = str_getcsv($row, ";");
            
            if (count($data) === 3) {
                $date = $data[0];
                $code = $data[1];
                $montant = $data[2];

                $idTypeDepense = $this->getIDDepense($code);

                if($idTypeDepense==0){
                    return redirect()->back()->with('erreurcode','Le fichier contient un code invalide : '.$code);
                }

                if($idTypeDepense==-1){
                    return redirect()->back()->with('erreurcode','Le fichier contient un type de dépense supprimé ou inexistant: '.$code);
                }

                if($this->isDateValid($date,'d/m/Y')==false){
                    return redirect()->back()->with('erreurcode','Le fichier contient une date invalide : '.$date);
                }

                if($montant<=0){
                    return redirect()->back()->with('erreurcode','Le fichier contient un montant invalide : '.$montant);
                }
            }
        }
        foreach ($rows as $row) {
            $data = str_getcsv($row, ";");
            
            if (count($data) === 3) {
                $date = $data[0];
                $code = $data[1];
                $montant = $data[2];

                $idTypeDepense = $this->getIDDepense($code);
                $value = session('utilisateur');
                $idUtilisateur = $value->id;
                
                DB::table('Depense')->insert([
                    'idUtilisateur' => $idUtilisateur,
                    'idTypeDepense' => $idTypeDepense,
                    'dateDepense' => $date,
                    'cout' => $montant,
                ]);
            }
        }
        return redirect('saisiedepense');
    }

    // FONCTIONS UTILES
    function isDateValid($dateString, $format = 'Y-m-d')
    {
        $dateObject = DateTime::createFromFormat($format, $dateString);
        return $dateObject && $dateObject->format($format) === $dateString;
    }

    public function getIDDepense($code){
        $idTypeDepense = 0;
        $typeDepense  = TypeDepense::where('code','=',$code)->get()->first();
        if($typeDepense!=null){
            $idTypeDepense = $typeDepense->id;
            if($typeDepense->etat==2){
                $idTypeDepense = -1;
            }
        }else{
            $idTypeDepense = -1;
        }
        return $idTypeDepense;
    }
 
}