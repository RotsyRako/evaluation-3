<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientGenre extends Model
{
    protected $table = 'V_PatientGenre';
    protected $fillable = [
        "id",
        "nom",
        "dateNaissance",
        "idGenre",
        "remboursement",
        "etat",
        "genre"
    ];
    public $timestamps = false;
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->fillable = array_merge($this->fillable, array_keys($attributes));
    }
    use HasFactory;
}
