<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DashboardBenefice extends Model
{
    //attribut pour recettes totales 
    public $sommerecettereelle;
    public $sommerecettebudgetmensuel;
    public $sommerecettebudrealisation;
    //attribut pour dépenses totaux 
    public $sommedepensereelle;
    public $sommedepensebudgetmensuel;
    public $sommedepensebudrealisation;
    //attribut pour benefice
    public $beneficereel;
    public $beneficebudget;
    public $beneficerealisation;

    public function initDashboardData($month, $year){
        //SOMME DE TOUTES LES DEPENSES
        $sumonth = Depense::whereYear('dateDepense', $year)
        ->whereMonth('dateDepense', $month)
        ->sum('cout');
        $this->sommedepensereelle = $sumonth;

        //SOMME DE TOUS LES BUDGETS MENSUELS DEPENSE
        $sumbug = TypeDepense::sum('budgetAnnuel');
        $this->sommedepensebudgetmensuel = $sumbug/12;

        //POURCENTAGE DE REALISATION DEPENSE
        $this->sommedepensebudrealisation = ($this->sommedepensereelle/$this->sommedepensebudgetmensuel)*100;
        
        //SOMME DE TOUTES LES RECETTES
        $sumonth = FactureFullDetail::whereYear('dateFacture', $year)
        ->whereMonth('dateFacture', $month)
        ->sum('prix');
        $this->sommerecettereelle = $sumonth;

        //SOMME DE TOUS LES BUDGETS MENSUELS RECETTE
        $sumbug = TypeActe::sum('budgetAnnuel');
        $this->sommerecettebudgetmensuel = $sumbug/12;

        //POURCENTAGE DE REALISATION RECETTE
        $this->sommerecettebudrealisation = ($this->sommerecettereelle/$this->sommerecettebudgetmensuel)*100;

        //BENEFICE REEL
        $this->beneficereel = $this->sommerecettereelle - $this->sommedepensereelle;

        //BENEFICE BUDGET MENSUEL
        $this->beneficebudget = $this->sommerecettebudgetmensuel - $this->sommedepensebudgetmensuel;

        //POURCENTAGE DE REALISATION
        $this->beneficerealisation = ($this->beneficereel/$this->beneficebudget)*100;
    }

    public function formatData(){        
        $this->sommerecettereelle=number_format($this->sommerecettereelle,2,'.',' ');
        $this->sommerecettebudgetmensuel=number_format($this->sommerecettebudgetmensuel,2,'.',' ');
        $this->sommerecettebudrealisation=number_format($this->sommerecettebudrealisation,2,'.',' ');
        
        $this->sommedepensereelle=number_format($this->sommedepensereelle,2,'.',' ');
        $this->sommedepensebudgetmensuel=number_format($this->sommedepensebudgetmensuel,2,'.',' ');
        $this->sommedepensebudrealisation=number_format($this->sommedepensebudrealisation,2,'.',' ');
        
        $this->beneficereel=number_format($this->beneficereel,2,'.',' ');
        $this->beneficebudget=number_format($this->beneficebudget,2,'.',' ');
        $this->beneficerealisation=number_format($this->beneficerealisation,2,'.',' ');
    }
}
