<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FacturePatient extends Model
{
    protected $table = 'V_FacturePatient';
    protected $fillable = [
        "id" ,
        "idPatient",
        "idUtilisateur",
        "dateFacture",
        "etat",
        "nom",
        "dateNaissance"
    ];
    public $timestamps = false;
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->fillable = array_merge($this->fillable, array_keys($attributes));
    }
    
    //TRADUCTION EN FR
    private function translateMonthToFrench($month)
    {
    $englishMonths = [
        'January', 'February', 'March', 'April', 'May', 'June', 'July',
        'August', 'September', 'October', 'November', 'December'
    ];

    $frenchMonths = [
        'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet',
        'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'
    ];

    // Find the corresponding French month for the given English month
    $index = array_search($month, $englishMonths);

    if ($index !== false) {
        return $frenchMonths[$index];
    }

    // If the month is not found, return the original month name
    return $month;
    }

        //FORMATTER LA DATE DE FACTURE
        public function getDateFactureFormatted()
        {
            $datetime = Carbon::parse($this->dateFacture);
            $month = $datetime->format('F');
            $frenchMonth = $this->translateMonthToFrench($month);
            $dateformat = $datetime->day." ".$frenchMonth." ".$datetime->year;
            return $dateformat;
        }
    
        //FORMATTER LA DATE DE NAISSANCE
        public function getDateNaissanceFormatted()
        {
            $datetime = Carbon::parse($this->dateNaissance);
            $month = $datetime->format('F');
            $frenchMonth = $this->translateMonthToFrench($month);
            $dateformat = $datetime->day." ".$frenchMonth." ".$datetime->year;
            return $dateformat;
        }
    
    
    // RELATION AVEC "FactureDetailActe"
    public function items()
    {
        return $this->hasMany(FactureDetailActe::class, 'idFacture', 'id');
    }

    //PRIX TOTAL DE LA FACTURE
    public function getTotalAmountAttribute()
    {
        $items = $this->items;

        $totalAmount = 0;

        foreach ($items as $item) {
            $unformattedPrix = (float) str_replace(' ', '', $item->prix);

            if (is_numeric($unformattedPrix)) {
                $totalAmount += $unformattedPrix;
            }
        }
        $formattedTotalAmount = number_format($totalAmount, 2, '.', ' ');
        
        return $formattedTotalAmount;
    }
    use HasFactory;
}
