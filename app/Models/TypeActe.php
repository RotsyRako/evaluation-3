<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeActe extends Model
{
    protected $table = 'TypeActe';
    protected $fillable = [
        "id",
        "typeActe",
        "budgetAnnuel",
        "code",
        "etat"
    ];
    public $timestamps = false;
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->fillable = array_merge($this->fillable, array_keys($attributes));
    }

    use HasFactory;
}
