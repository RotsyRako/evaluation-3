<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UtilisateurType extends Model
{
    protected $table = 'V_UtilisateurType';
    protected $fillable = [
        "id",
        "idTypeUtilisateur",
        "nom",
        "login",
        "mdp",
        "nomType"
    ];
    public $timestamps = false;
    use HasFactory;
    
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->fillable = array_merge($this->fillable, array_keys($attributes));
    }

    //VERIFIER SI L'UTILISATEUR EXISTE
    public function getUtilisateur(){
        $allUtilisateurType=$this::all();
        foreach($allUtilisateurType as $utilisateur){
            if($utilisateur->login==$this->login && $utilisateur->mdp == $this->mdp){
                return $utilisateur;
            }
        }
        return null;
    }
}
