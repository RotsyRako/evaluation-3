<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FactureFullDetail extends Model
{
    protected $table = 'V_FactureFullDetail';
    protected $fillable = [
        "id",
        "idPatient",
        "idUtilisateur",
        "dateFacture",
        "etat",
        "nom",
        "dateNaissance",
        "idFactureDetail",
        "idTypeActe",
        "prix",
        "typeActe",
        "budgetAnnuel",
        "code"
    ];
    public $timestamps = false;
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->fillable = array_merge($this->fillable, array_keys($attributes));
    }

    //TRADUCTION EN FR
    private function translateMonthToFrench($month)
    {
    $englishMonths = [
        'January', 'February', 'March', 'April', 'May', 'June', 'July',
        'August', 'September', 'October', 'November', 'December'
    ];

    $frenchMonths = [
        'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet',
        'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'
    ];

    // Find the corresponding French month for the given English month
    $index = array_search($month, $englishMonths);

    if ($index !== false) {
        return $frenchMonths[$index];
    }

    // If the month is not found, return the original month name
    return $month;
    }

    //FORMATTER LA DATE DE FACTURE
    public function getDateFactureFormatted()
    {
        $datetime = Carbon::parse($this->dateFacture);
        $month = $datetime->format('F');
        $frenchMonth = $this->translateMonthToFrench($month);
        $dateformat = $datetime->day." ".$frenchMonth." ".$datetime->year;
        return $dateformat;
    }

    //FORMATTER LA DATE DE NAISSANCE
    public function getDateNaissanceFormatted()
    {
        $datetime = Carbon::parse($this->dateNaissance);
        $month = $datetime->format('F');
        $frenchMonth = $this->translateMonthToFrench($month);
        $dateformat = $datetime->day." ".$frenchMonth." ".$datetime->year;
        return $dateformat;
    }

    use HasFactory;
}
