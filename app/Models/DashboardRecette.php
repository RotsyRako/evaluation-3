<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DashboardRecette extends Model
{
    //attributs par idTyperecette, mois d'une année
    public $recettereelle; 
    public $recettebudgetmensuel;
    public $realisationrecette;
    
    public function initDashboardData($idTypeActe, $month, $year){
        //SOMME DES RECETTES POUR UN TYPE DE RECETTE
        $result = FactureFullDetail::where('idTypeActe', $idTypeActe)
        ->whereYear('dateFacture', $year)
        ->whereMonth('dateFacture', $month)
        ->get();
        $sumOfPrices = $result->sum('prix');

        if($sumOfPrices!=null){
            $this->recettereelle = $sumOfPrices;
        }else{
            $this->recettereelle =0;
        }

        //BUDGET PAR MOIS POUR UN TYPE DE RECETTE
        $type = TypeActe::where('id', $idTypeActe);
        $budget = $type->first()->budgetAnnuel;
        $budgetmensuel =(float)($budget/12);
        $this->recettebudgetmensuel = $budgetmensuel;

        //POURCENTAGE DE REALISATION POUR UNE RECETTE
        $rea = ($this->recettereelle/$this->recettebudgetmensuel)*100;
        $this->realisationrecette = $rea;
    }

    public function formatData(){
        $this->recettereelle=number_format($this->recettereelle,2,'.',' ');
        $this->recettebudgetmensuel=number_format($this->recettebudgetmensuel,2,'.',' ');
        $this->realisationrecette=number_format($this->realisationrecette,2,'.',' ');
    }
}