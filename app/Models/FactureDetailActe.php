<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FactureDetailActe extends Model
{
    protected $table = 'V_FactureDetailTypeActe';
    protected $fillable = [
        "id" ,
        "idFacture",
        "idTypeActe",
        "prix",
        "typeActe",
        "budgetAnnuel",
        "code",
        "etat"
    ];
    public $timestamps = false;
    
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->fillable = array_merge($this->fillable, array_keys($attributes));
    }

    //ATTRIBUT "PRIX" FORMATTE
    public function getPrixAttribute($value)
    {
        return number_format($value, 2, '.', ' ');
    }

    //BUDGET ANNUEL FORMATTE
    public function getBudgetAnnuelAttribute($value)
    {
        return number_format($value, 2, '.', ' ');
    }
    use HasFactory;
}
