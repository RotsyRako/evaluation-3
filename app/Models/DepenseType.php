<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepenseType extends Model
{
    protected $table = 'V_DepenseType';
    protected $fillable = [
        "id",
        "idUtilisateur",
        "idTypeDepense",
        "dateDepense",
        "cout",
        "etat",
        "typeDepense",
        "budgetAnnuel",
        "code"
    ];
    public $timestamps = false;
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->fillable = array_merge($this->fillable, array_keys($attributes));
    }

    //TRADUCTION EN FR
    private function translateMonthToFrench($month)
    {
    $englishMonths = [
        'January', 'February', 'March', 'April', 'May', 'June', 'July',
        'August', 'September', 'October', 'November', 'December'
    ];

    $frenchMonths = [
        'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet',
        'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'
    ];

    // Find the corresponding French month for the given English month
    $index = array_search($month, $englishMonths);

    if ($index !== false) {
        return $frenchMonths[$index];
    }

    // If the month is not found, return the original month name
    return $month;
    }

    //FORMATTER LES DATES
    public function getDateFormatted()
    {
        $datetime = Carbon::parse($this->dateDepense);
        $month = $datetime->format('F');
        $frenchMonth = $this->translateMonthToFrench($month);
        $dateformat = $datetime->day." ".$frenchMonth." ".$datetime->year;
        return $dateformat;
    }
    use HasFactory;
}
