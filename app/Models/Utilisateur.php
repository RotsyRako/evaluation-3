<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Utilisateur extends Model
{
    protected $table = 'Utilisateur';
    protected $fillable = [
        "id" ,
        "idTypeUtilisateur",
        "nom",
        "login",
        "mdp",
        "etat"
    ];
    public $timestamps = false;
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->fillable = array_merge($this->fillable, array_keys($attributes));
    }
    use HasFactory;
}
