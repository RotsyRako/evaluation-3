<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DashboardDepense extends Model
{
    //attributs par idTypeDepense, mois d'une année
    public $depensereelle; 
    public $depensebudgetmensuel;
    public $realisationdepense;
    
    public function initDashboardData($idTypeDepense, $month, $year){
        //SOMME DES DEPENSES POUR UN TYPE DE DEPENSE
        $sum = Depense::where('idTypeDepense', $idTypeDepense)
        ->whereYear('dateDepense', $year)
        ->whereMonth('dateDepense', $month)
        ->sum('cout');

        $this->depensereelle = $sum;

        //BUDGET PAR MOIS POUR UN TYPE DE DEPENSE
        $type = TypeDepense::where('id', $idTypeDepense);
        $budget = $type->first()->budgetAnnuel;
        $budgetmensuel =(float)($budget/12);
        $this->depensebudgetmensuel = $budgetmensuel;

        //POURCENTAGE DE REALISATION POUR UNE DEPENSE
        $rea = ($this->depensereelle/$this->depensebudgetmensuel)*100;
        $this->realisationdepense = $rea;
    }

    public function formatData(){
        $this->depensereelle=number_format($this->depensereelle,2,'.',' ');
        $this->depensebudgetmensuel=number_format($this->depensebudgetmensuel,2,'.',' ');
        $this->realisationdepense=number_format($this->realisationdepense,2,'.',' ');
    }
}
