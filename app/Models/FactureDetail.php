<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FactureDetail extends Model
{
    protected $table = 'FactureDetail';
    protected $fillable = [
        "id" ,
        "idFacture",
        "idTypeActe",
        "prix"
    ];
    public $timestamps = false;
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->fillable = array_merge($this->fillable, array_keys($attributes));
    }

    //ATTRIBUT "PRIX" DIRECTEMENT FORMATTE
    public function getPrixAttribute($value)
    {
        return number_format($value, 2, '.', ' ');
    }
    use HasFactory;
}
