------------------ DROP ------------------------
DROP TABLE "DepenseDetail" CASCADE;
DROP TABLE "Depense" CASCADE;
DROP TABLE "FactureDetail" CASCADE;
DROP TABLE "Facture" CASCADE;
DROP TABLE "TypeDepense" CASCADE;
DROP TABLE "TypeActe" CASCADE;
DROP TABLE "Patient" CASCADE;
DROP TABLE "Genre" CASCADE;
DROP TABLE "Utilisateur" CASCADE;
DROP TABLE "Etat" CASCADE;
DROP TABLE "TypeUtilisateur" CASCADE;

------------------ INSERTION -------------------

INSERT INTO "TypeUtilisateur"("nomType") VALUES
('Administrateur'),
('Utilisateur');

INSERT INTO "Etat"("etat") VALUES
('actif'),
('supprime');

INSERT INTO "Utilisateur"("idTypeUtilisateur","nom","login","mdp") VALUES
(1,'Ben Son','bens','123'),
(2,'Karl Marx','karlm','123'),
(2,'Dean Lewis','deanl','123');

INSERT INTO "Genre"("genre") VALUES
('Homme'),
('Femme');

INSERT INTO "Patient"("nom","dateNaissance","idGenre","remboursement") VALUES
('RAKOTO Jean','1999-02-16',1,FALSE),
('RAVAO Maria','2000-12-03',2,FALSE),
('RASOA Tiana','1996-03-24',2,FALSE);

INSERT INTO "TypeActe"("typeActe","budgetAnnuel","code") VALUES
('Consultation',10000000,'CON'),
('Medicament',10000000,'MED'),
('Operation',10000000,'OPE');

INSERT INTO "TypeDepense"("typeDepense","budgetAnnuel","code") VALUES
('Loyer',12000000,'LOY'),
('Salaire',12000000,'SAL');

