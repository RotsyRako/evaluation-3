CREATE DATABASE clinique;
\c clinique

--------------- CREATE TABLE -----------------

CREATE TABLE "TypeUtilisateur"(
    "id" SERIAL NOT NULL PRIMARY KEY,
    "nomType" VARCHAR(30)
);

CREATE TABLE "Etat"(
    "id" SERIAL NOT NULL PRIMARY KEY,
    "etat" VARCHAR(30)
);

CREATE TABLE "Utilisateur"(
    "id" SERIAL NOT NULL PRIMARY KEY,
    "idTypeUtilisateur" INT,
    "nom" VARCHAR(30),
    "login" VARCHAR(30),
    "mdp" VARCHAR(30),
    "etat" INT DEFAULT 1,
    FOREIGN KEY("etat") REFERENCES "Etat"("id"),
    FOREIGN KEY("idTypeUtilisateur") REFERENCES "TypeUtilisateur"("id")
);

CREATE TABLE "Genre"(
    "id" SERIAL NOT NULL PRIMARY KEY,
    "genre" VARCHAR(10)
);

CREATE TABLE "Patient"(
    "id" SERIAL NOT NULL PRIMARY KEY,
    "nom" VARCHAR(30),
    "dateNaissance" DATE,
    "idGenre" INT,
    "remboursement" BOOLEAN,
    "etat" INT DEFAULT 1,
    FOREIGN KEY("etat") REFERENCES "Etat"("id"),
    FOREIGN KEY("idGenre") REFERENCES "Genre"("id")
);

CREATE TABLE "TypeActe"(
    "id" SERIAL NOT NULL PRIMARY KEY,
    "typeActe" VARCHAR(30),
    "budgetAnnuel" DECIMAL(10,2),
    "code" VARCHAR(3),
    "etat" INT DEFAULT 1,
    FOREIGN KEY("etat") REFERENCES "Etat"("id")
);

CREATE TABLE "TypeDepense"(
    "id" SERIAL NOT NULL PRIMARY KEY,
    "typeDepense" VARCHAR(30),
    "budgetAnnuel" DECIMAL(10,2),
    "code" VARCHAR(3),
    "etat" INT DEFAULT 1,
    FOREIGN KEY("etat") REFERENCES "Etat"("id")
);

CREATE TABLE "Facture"(
    "id" SERIAL NOT NULL PRIMARY KEY,
    "idPatient" INT,
    "idUtilisateur" INT,
    "dateFacture" TIMESTAMP,
    "etat" INT DEFAULT 1,
    FOREIGN KEY("etat") REFERENCES "Etat"("id"),
    FOREIGN KEY ("idPatient") REFERENCES "Patient"("id"),
    FOREIGN KEY ("idUtilisateur") REFERENCES "Utilisateur"("id")
);

CREATE TABLE "FactureDetail"(
    "id" SERIAL NOT NULL PRIMARY KEY,
    "idFacture" INT,
    "idTypeActe" INT,
    "prix" DECIMAL(10,2),
    FOREIGN KEY("idFacture") REFERENCES "Facture"("id"),
    FOREIGN KEY("idTypeActe") REFERENCES "TypeActe"("id")
);

CREATE TABLE "Depense"(
    "id" SERIAL NOT NULL PRIMARY KEY,
    "idUtilisateur" INT,
    "idTypeDepense" INT,
    "dateDepense" TIMESTAMP,
    "cout" DECIMAL(10,2),
    "etat" INT DEFAULT 1,
    FOREIGN KEY("etat") REFERENCES "Etat"("id"),
    FOREIGN KEY ("idUtilisateur") REFERENCES "Utilisateur"("id"),
    FOREIGN KEY ("idTypeDepense") REFERENCES "TypeDepense"("id")
);

--------------- VIEWS ------------------
CREATE OR REPLACE VIEW "V_UtilisateurType" AS
SELECT u.*,tu."nomType" FROM "Utilisateur" u JOIN "TypeUtilisateur" tu ON u."idTypeUtilisateur"=tu."id";

CREATE OR REPLACE VIEW "V_PatientGenre" AS
SELECT p.*, g."genre" FROM "Patient" p JOIN "Genre" g ON p."idGenre"=g."id";

CREATE OR REPLACE VIEW "V_FacturePatient" AS
SELECT f.*, p."nom", p."dateNaissance" FROM "Facture" f JOIN "Patient" p ON f."idPatient" = p."id";

CREATE OR REPLACE VIEW "V_FactureDetailTypeActe" AS
SELECT fd.*, t."typeActe",t."budgetAnnuel",t."code",t."etat" FROM "FactureDetail" fd JOIN "TypeActe" t ON fd."idTypeActe"=t."id";

CREATE OR REPLACE VIEW "V_FactureFullDetail" AS
SELECT fp.*, fdt."id" as "idFactureDetail", fdt."idTypeActe", fdt."prix", fdt."typeActe",fdt."budgetAnnuel",fdt."code" FROM "V_FacturePatient" fp JOIN "V_FactureDetailTypeActe" fdt ON fp."id" = fdt."idFacture";

CREATE OR REPLACE VIEW "V_DepenseType" AS
SELECT d.*,td."typeDepense",td."budgetAnnuel",td."code" FROM "Depense" d JOIN "TypeDepense" td ON d."idTypeDepense" = td."id";

--select sum("prix") as aggregate from "V_FactureFullDetail" where "idTypeActe" = 1 and extract(year from "dateFacture") = 2023 and extract(month from "dateFacture") = 7
