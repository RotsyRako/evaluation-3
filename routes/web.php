<?php

use App\Http\Controllers\FormController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('identify');
});
Route::get('deconnexion',[FormController::class,'deconnexion']);
Route::post('/authentification',[FormController::class,'authentification']);

Route::get('/gestionpatient',[FormController::class,'gestionpatient']);
Route::get('/gestiontypeacte',[FormController::class,'gestiontypeacte']);
Route::get('/gestiontypeacte',[FormController::class,'gestiontypeacte'])->name('gestiontypeacte');
Route::get('/gestiontypedepense',[FormController::class,'gestiontypedepense']);
Route::get('/gestiontypedepense',[FormController::class,'gestiontypedepense'])->name('gestiontypedepense');

Route::get('/saisiefacture',[FormController::class,'saisiefacture']);
Route::get('/listefactures',[FormController::class,'listefactures']);
Route::post('/detailfacture/{id}',[FormController::class,'detailfacture']);

Route::get('/saisiedepense',[FormController::class,'saisiedepense']);
Route::post('/savedepense',[FormController::class,'savedepense']);
Route::get('/listedepenses',[FormController::class,'listedepenses']);
Route::post('/multidepense',[FormController::class,'multidepense']);

Route::post('/saveInvoice',[FormController::class,'saveInvoice']);

Route::post('/ajouterpatient',[FormController::class,'ajouterpatient']);
Route::post('/modifierpatient',[FormController::class,'modifierpatient']);
Route::post('/supprimerpatient/{id}',[FormController::class,'supprimerpatient']);

Route::post('/ajoutertypeacte',[FormController::class,'ajoutertypeacte']);
Route::post('/modifiertypeacte',[FormController::class,'modifiertypeacte']);
Route::post('/supprimertypeacte/{id}',[FormController::class,'supprimertypeacte']);

Route::post('/ajoutertypedepense',[FormController::class,'ajoutertypedepense']);
Route::post('/modifiertypedepense',[FormController::class,'modifiertypedepense']);
Route::post('/supprimertypedepense/{id}',[FormController::class,'supprimertypedepense']);

Route::post('/importcsv',[FormController::class,'importcsv']);

Route::get('/nouveautableaubord',[FormController::class,'nouveautableaubord']);
Route::post('/nouveauloadMonthData',[FormController::class,'nouveauloadMonthData']);