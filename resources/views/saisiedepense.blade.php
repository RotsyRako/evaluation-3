@extends('layouts.utilisateur')

@section('title', 'Saisie de dépense')
@section('content')

<div class="container-fluid">
    <h3 class="text-dark mb-1" style="margin-top: 50px;">Saisie de dépense</h3>
</div>

<div class="table-responsive" style="background: #ffffff;margin-top: 25px;width: 850px;margin-left: 25px;">
<form action="savedepense" method="post">
@csrf    
<table class="table">
        <thead>
            <tr>
                <th style="color: rgb(31,32,41);width: auto;">Détails</th>
                <th style="color: rgb(31,32,41);width: auto;"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><label class="form-label" style="border-color: rgb(133, 135, 150);border-top-color: rgb(133,;border-right-color: 135,;border-bottom-color: 150);border-left-color: 135,;">Type de dépense</label></td>
                <td><select name="typedep" style="border-style: solid;border-color: var(--bs-gray-200);border-radius: 15px;">
                    @foreach($dep as $p)
                        @if($p->etat!=2)
                        <option value="{{$p->id}}">{{$p->typeDepense}}</option>
                        @endif
                    @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <td><label class="form-label">Cout</label></td>
                <td><input type="number" name="cout" step="any" min=0 style="border-style: solid;border-color: var(--bs-gray-200);border-radius: 25px;"></td>
            </tr>
            <tr>
                <td><label class="form-label">Date</label></td>
                <td><input type="datetime-local" name="datedep" value="{{ now()->format('Y-m-d\TH:i') }}" style="border-style: solid;border-color: var(--bs-gray-200);border-radius: 15px;"></td>
            </tr>
            <tr>
                <td></td>
                <td><button class="btn btn-primary" type="submit" style="background: #172a3a;border-style: none;">Ajouter</button></td>
                <td></td>
            </tr>
        </tbody>
        </form>
    </table>
</div>

<div class="container-fluid">
    <h3 class="text-dark mb-1" style="margin-top: 50px;">Saisie multiple</h3>
</div>

<div class="table-responsive" style="background: #ffffff;margin-top: 25px;width: 850px;margin-left: 25px;">
<form action="multidepense" method="post">
@csrf    
<table class="table">
        <thead>
            <tr>
                <th style="color: rgb(31,32,41);width: auto;">Détails</th>
                <th style="color: rgb(31,32,41);width: auto;"></th>
            </tr>
        </thead>
        <tbody>
            @if(Session::get('erreurcode')!=null)
            @php
            $code =Session::get('erreurcode');
            @endphp
            <tr>
                <td>
                    @for($i=0; $i<count($code); $i++)
                    <p style="color: red">Echec d'insertion : {{$code[$i]}} invalide</p>
                    @endfor    
            </td>
            </tr>            
            @endif
            <tr>
                <td><label class="form-label" style="border-color: rgb(133, 135, 150);border-top-color: rgb(133,;border-right-color: 135,;border-bottom-color: 150);border-left-color: 135,;">Type de dépense</label></td>
                <td>
                    <input type="text" name="typedep" style="border-style: solid;border-color: var(--bs-gray-200);border-radius: 25px;">
                <!--    <select name="typedep" style="border-style: solid;border-color: var(--bs-gray-200);border-radius: 15px;">
                    @foreach($dep as $p)
                        @if($p->etat!=2)
                        <option value="{{$p->id}}">{{$p->typeDepense}}</option>
                        @endif
                    @endforeach
                    </select> -->
                </td>
            </tr>
            @if(Session::get('erreurdate')!=0)
            <tr>
                <td>
                <p style="color: red">Echec d'insertion : veuillez ne saisir que des dates valides.</p>
                </td>
            </tr>            
            @endif
            <tr>
                <td><label class="form-label">Jour</label></td>
                <td><input type="number" name="jour" max=31 min=1 value="1" style="border-style: solid;border-color: var(--bs-gray-200);border-radius: 25px;"></td>
                <tr>
                    <td><label class="form-label">Mois</label></td>
                    <td>
                        @foreach($monthNames as $monthValue => $monthName)
                        <label>
                            <input type="checkbox" name="months[]" value="{{ $monthValue+1 }}">
                            {{ $monthName }}
                        </label>
                            <br>
                        @endforeach
                    </td>
                </tr>
            </tr>
            <tr>
                <td><label class="form-label">Année</label></td>
                <td><input type="number" name="annee" min=1 value="2023" style="border-style: solid;border-color: var(--bs-gray-200);border-radius: 25px;"></td>
            </tr>
            @if(Session::get('erreurmontant')!=null)
            @php
            $data =Session::get('erreurmontant');
            @endphp
            <tr>
                <td>
                    @for($i=0; $i<count($data);$i++)
                    <p style="color: red">Echec d'insertion : {{$data[$i]}} invalide</p>
                    @endfor    
            </td>
            </tr>            
            @endif
            <tr>
                <td><label class="form-label">Montant</label></td>
                <td><input required type="text" name="montant" style="border-style: solid;border-color: var(--bs-gray-200);border-radius: 25px;">Ar</td>
            </tr>
            <tr>
                <td></td>
                <td><button class="btn btn-primary" type="submit" style="background: #172a3a;border-style: none;">Ajouter</button></td>
                <td></td>
            </tr>
        </tbody>
        </form>
    </table>
</div>

<div class="container-fluid">
    <h3 class="text-dark mb-1" style="margin-top: 50px;">Importer des données (CSV) </h3>
</div>

<div class="table-responsive" style="background: #ffffff;margin-top: 25px; margin-bottom:25px;width: 850px;margin-left: 25px;">
<form action="importcsv" method="post" enctype="multipart/form-data">
@csrf    
<table class="table">
        <thead>
            <tr>
                <th style="color: rgb(31,32,41);width: auto;">Veuillez importer le fichier</th>
                <th style="color: rgb(31,32,41);width: auto;"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><label class="form-label" style="border-color: rgb(133, 135, 150);border-top-color: rgb(133,;border-right-color: 135,;border-bottom-color: 150);border-left-color: 135,;">Type de dépense</label></td>
                <td><input type="file" name="csv_file"></td>
            </tr>
            @if(Session::has('err'))
            <p style="color: red">{{Session::get('err')}}</p>
            <tr>
                <td>
                </td>
            </tr>            
            @endif
            <tr>
                <td></td>
                <td><button class="btn btn-primary" type="submit" style="background: #172a3a;border-style: none;">Importer</button></td>
                <td></td>
            </tr>
        </tbody>
        </form>
    </table>
</div>
@endsection