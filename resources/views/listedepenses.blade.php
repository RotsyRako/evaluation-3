@extends('layouts.admin')

@section('title', 'Toutes les dépenses')
@section('content')
<div class="container-fluid">
    <h3 class="text-dark mb-1" style="margin-top: 50px;">Gestion des types de dépenses</h3>
</div>
<div class="table-responsive" style="background: #ffffff;margin-top: 25px;width: 950px;margin-left: 25px;">
    <table class="table">
        <thead>
            <tr>
                <th style="color: rgb(31,32,41);width: auto;">Types de dépense</th>
                <th style="color: rgb(31,32,41);width: auto;"></th>
                <th style="color: rgb(31,32,41);width: auto;"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <form action="ajoutertypedepense" method="post">
                @csrf
                <td><input type="text" name="depense" placeholder="Type de dépense" style="border-style: none;border-radius: 25px;background: rgb(255, 255, 255);"></td>
                <td><input type="text" name="code" placeholder="Code" style="border-style: none;border-radius: 25px;background: rgb(255, 255, 255);"></td>
                <td><input type="number" step="any" name="budgetannuel" placeholder="Budget annuel" style="border-style: none;border-radius: 25px;background: rgb(255, 255, 255);"></td>
                <td><button class="btn btn-primary" type="submit" style="background: #172a3a;border-style: none;">Ajouter</button></td>
                </form>
                @if (request()->query('erreur') === 'code')
                <p style="color: red">Echec d'insertion : le code doit être unique.</p>
                @endif
            </tr>
            @if($toutesdepenses!=null)
            @foreach($toutesdepenses as $dep)
            @if($dep->etat!=2)
            <tr>
                <form action="modifiertypedepense" method="post">
                @csrf
                <input type="hidden" name="id" value="{{$dep->id}}">
                <td><input type="text" name="typedep" value="{{$dep->typeDepense}}" style="border-style: none;border-radius: 25px;background: rgb(255, 255, 255);"></td>
                <td><input type="text" name="code" value="{{$dep->code}}" style="border-style: none;border-radius: 25px;background: rgb(255, 255, 255);"></td>
                <td><input type="number" step="0.01" name="budgetannuel" value="{{$dep->budgetAnnuel}}" style="border-style: none;border-radius: 25px;background: rgb(255, 255, 255);">Ar</td>
                <td><button class="btn btn-primary" type="submit" style="background: #7C7A7A;border-style: none;">Modifier</button></td>
                </form>
                <td>
                <form action="supprimertypedepense/{{$dep->id}}" method="post">
                    @csrf
                    <button class="btn btn-primary" type="submit" style="background: #551B14;border-style: none;">Supprimer</button></td>
                </form>            
                </tr>
            @endif
            @endforeach
        @endif
        </tbody>
    </table>
</div>

@endsection
