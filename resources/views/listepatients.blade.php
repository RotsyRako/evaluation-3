@extends('layouts.admin')

@section('title', 'Tous les patients')
@section('content')
<div class="container-fluid">
    <h3 class="text-dark mb-1" style="margin-top: 50px;">Gestion des patients</h3>
</div>
<div class="table-responsive" style="background: #ffffff;margin-top: 25px;">
<table class="table">
    <thead>
        <tr>
            <th style="color: rgb(31,32,41);width: auto;">Nom &amp; prénom(s)</th>
            <th style="color: rgb(31,32,41);width: auto;">Date de naissance</th>
            <th style="color: rgb(31,32,41);width: auto;">Genre</th>
            <th style="color: rgb(31,32,41);width: auto;">Remboursement</th>
            <th style="color: rgb(31,32,41);width: auto;"></th>
            <th style="color: rgb(31,32,41);width: auto;"></th>
        </tr>
    </thead>
        <tr>
        <form action="ajouterpatient" method="post">
        @csrf
            <td><input type="text" name="nom" placeholder="Nom & prénom(s)" style="border-style: none;border-radius: 25px;background: rgb(255, 255, 255);"></td>
            <td><input type="date" name="naissance" placeholder="Date de naissance" style="border-radius: 5px;border-style: none;"></td>
            <td><select name="genre" style="border-style: none;border-radius: 10px;">
            @foreach($genre as $g)
            <option value="{{$g->id}}">{{$g->genre}}</option>
            @endforeach
                </select>
            </td>
            <td>
                <select name="remboursement" style="border-style: none;border-radius: 10px;">
                    <option value="true">Oui</option>
                    <option value="false">Non</option>
                </select>
            </td>
            <td><button class="btn btn-primary" value="Ajouter" type="submit" style="background: #172a3a;border-style: none;">Ajouter</button></td>
        </form>
        @if (isset($error))
            <p style="color: red">Date de naissance invalide, veuillez vérifier à nouveau.</p>
        @endif
        </tr>

        @if($allpatients!=null)
        @foreach($allpatients as $patient)
            @if($patient->etat!=2)
            <tr>
            <form action="modifierpatient" method="post">
               @csrf
               <input type="hidden" name="id" value="{{$patient->id}}">
            <td><input type="text" name="nom" value="{{$patient->nom}}" style="border-style: none;border-radius: 25px;background: rgb(255, 255, 255);"></td>
            <td><input type="date" name="naissance" value="{{$patient->dateNaissance}}" style="border-radius: 5px;border-style: none;"></td>
            <td><select name="genre" style="border-style: none;border-radius: 10px;">
                @foreach($genre as $g)
                @if($patient->genre==$g->genre)
                <option value="{{$g->id}}" selected>{{$g->genre}}</option>
                @else
                <option value="{{$g->id}}">{{$g->genre}}</option>
                @endif
                @endforeach
                </select></td>
            <td><select name="remboursement" style="border-style: none;border-radius: 10px;">
                @if($patient->remboursement==1)
                <option value="true" selected>Oui</option>
                <option value="false">Non</option>
                @else
                <option value="true">Oui</option>
                <option value="false" selected>Non</option>
                @endif
                </select></td>
            <td>
                <button class="btn btn-primary" type="submit" value="Modifier" style="background: #7c7a7a;border-style: none;"><i class="fa fa-pencil"></i></button>
                </form>
            </td>
            <td>
                <form action="supprimerpatient/{{$patient->id}}" method="post">
                    @csrf
                    <button class="btn btn-primary" type="submit" style="margin-left: 10px;border-style: none;background: #551b14;"><i class="fa fa-trash"></i></button>
                </form>
            </td>
        </tr>
        @endif
    @endforeach
    @endif
    </table>
    </div>
@endsection
