@extends('layouts.utilisateur')

@section('title', 'Saisie de facture')
@section('content')

<div class="container-fluid">
    <h3 class="text-dark mb-1" style="margin-top: 50px;">Toutes les factures</h3>
</div>
<div class="table-responsive" style="background: #ffffff;margin-top: 25px;width: 850px;margin-left: 25px;">
    <table class="table">
        <thead>
            <tr>
                <th style="color: rgb(31,32,41);width: auto;">Nom &amp; prénom(s) du patient</th>
                <th style="color: rgb(31,32,41);width: auto;">Date de facturation</th>
                <th style="color: rgb(31,32,41);width: auto;"></th>
            </tr>
        </thead>
        <tbody>
        @if($allfact!=null)
        @foreach($allfact as $fact)
            <tr>
                <td><label class="form-label" style="border-color: rgb(133, 135, 150);border-top-color: rgb(133,;border-right-color: 135,;border-bottom-color: 150);border-left-color: 135,;">{{$fact->nom}}</label></td>
                <td>{{$fact->getDateFactureFormatted()}}<br></td>
                <td>
                <form action="detailfacture/{{$fact->id}}" method="post">
                    @csrf
                    <button class="btn btn-primary" type="submit" style="background: #172a3a;border-style: none;">Détails</button><br></td>
                </form>          
                </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            @endforeach
    @endif
        </tbody>
    </table>
</div>
@endsection