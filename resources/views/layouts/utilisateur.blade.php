<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="/fonts/fontawesome5-overrides.min.css">
</head>

<body id="page-top">
    <div id="wrapper">
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0" style="background: #172a3a;">
            <div class="container-fluid d-flex flex-column p-0"><a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
                    <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-stethoscope"></i></div>
                    <div class="sidebar-brand-text mx-3"><span style="margin-left: -10px;">Rotsy Clinique</span></div>
                </a>
                <hr class="sidebar-divider my-0">
                <ul class="navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item"><a class="nav-link" href="/saisiefacture"><i class="fa fa-edit"></i><span>Saisie d'acte</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/saisiedepense"><i class="fa fa-shopping-cart"></i><span>Saisie de dépense</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/listedepenses"><i class="fa fa-th-list"></i><span>Toutes les dépenses</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/listefactures"><i class="fa fa-th-list"></i><span>Toutes les factures</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="/deconnexion"><i class="fa fa-sign-out"></i><span>Déconnexion</span></a></li>
                </ul>
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <div class="container-fluid" style="margin-top: 50px;">
                    @yield('content')
                </div>
            </div>
            <footer class="bg-white sticky-footer" style="border-style: none;">
                <div class="container my-auto">
                    <div class="text-center my-auto copyright"><span>Copyright © Rotsy Rako - 2023</span></div>
                </div>
            </footer>
        </div>
    </div>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/js/script.min.js"></script>
</body>

</html>