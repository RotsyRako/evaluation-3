@extends('layouts.utilisateur')

@section('title', 'Saisie de facture')
@section('content')
<div class="container-fluid">
    <h3 class="text-dark mb-1" style="margin-top: 50px;">Facture</h3>
</div>

<div class="table-responsive" style="background: #ffffff;margin-top: 25px;width: 850px;margin-left: 25px;">
<form action="saveInvoice" method="post">
    @csrf    
<table class="table" id="factureTable">
        <thead>
            <tr>
                <th style="color: rgb(31,32,41);width: auto;">Détails de la facture</th>
                <th style="color: rgb(31,32,41);width: auto;"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><label class="form-label" style="border-color: rgb(133, 135, 150);border-top-color: rgb(133,;border-right-color: 135,;border-bottom-color: 150);border-left-color: 135,;">Nom du patient</label></td>
                <td><select name="patient" style="border-style: solid;border-color: var(--bs-gray-200);border-radius: 15px;">
                    @foreach($allusers as $p)
                    @if($p->etat!=2)
                        <option value="{{$p->id}}">{{$p->nom}}</option>
                    @endif
                    @endforeach
                    </select></td>
            </tr>
            <tr>
                <td><label class="form-label">Date</label></td>
                <td><input type="datetime-local" name="datefacture" value="{{ now()->format('Y-m-d\TH:i') }}" style="border-style: solid;border-color: var(--bs-gray-200);border-radius: 15px;"></td>
            </tr>
            <tr>
                </table>
                <table id="factureRows" >
                    <tr id="typeActeRow">
                    <td><label class="form-label">Type d'acte</label></td>
                    <td><select name="typeActe[]" style="border-style: solid;border-color: var(--bs-gray-200);border-radius: 15px;">
                    @foreach($types as $t)
                        @if($t->etat != 2)
                        <option value="{{$t->id}}">{{$t->typeActe}}</option>
                        @endif
                    @endforeach
                        </select></td>
                    </tr>
                    <tr id="prixRow">
                        <td><label class="form-label">Prix</label></td>
                        <td>
                            <input type="number" name="cout[]" step="any" min="0" style="border-style: solid;border-color: var(--bs-gray-200);border-radius: 25px;">
                        </td>
                    </tr>
                </table>
            </tr>
            <br>
            <tr>
                <td><button class="btn btn-primary" type="button" onclick="addRow()" style="background: #7C7A7A;border-style: none;">Autre</button></td>
                <td><button class="btn btn-primary" type="submit" style="background: #172a3a;border-style: none;">Valider la facture</button></td>
                <td></td>
            </form>
            </tr>
        </tbody>
</div>
<script>
        const maxRowCount = 50; // Specify the maximum number of rows

        function addRow() {
            const rowCount = document.getElementById('factureRows').rows.length;

            if (rowCount >= maxRowCount) {
                alert(`Maximum ${maxRowCount} rows are allowed.`);
                return;
            }

            // Clone the typeActeRow and prixRow
            const typeActeRow = document.getElementById('typeActeRow').cloneNode(true);
            const prixRow = document.getElementById('prixRow').cloneNode(true);

            // Generate unique IDs for the cloned rows
            typeActeRow.id = '';
            prixRow.id = '';

            // Clear the selected option in the cloned typeActeRow
            const selectElement = typeActeRow.querySelector('select[name="typeActe[]"]');
            selectElement.selectedIndex = 0;

            // Clear the value in the cloned prixRow
            const inputElement = prixRow.querySelector('input[name="cout[]"]');
            inputElement.value = '';

            // Create a delete button for the new row
            const deleteButton = document.createElement('input');
            deleteButton.type = 'button';
            deleteButton.value = 'Delete';
            deleteButton.onclick = function () {
                deleteRow(deleteButton);
            };

            // Append the delete button to the new row
            const deleteCell = document.createElement('td');
            deleteCell.appendChild(deleteButton);
            prixRow.appendChild(deleteCell);

            // Append the cloned rows to the table
            const table = document.getElementById('factureRows');
            table.appendChild(typeActeRow);
            table.appendChild(prixRow);
        }

        function deleteRow(button) {
            const row = button.parentNode.parentNode;
            const typeActeRow = row.previousElementSibling;
            typeActeRow.parentNode.removeChild(typeActeRow);
            row.parentNode.removeChild(row);
        }
    </script>
@endsection
