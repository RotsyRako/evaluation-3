@extends('layouts.admin')

@section('title', 'Tous les actes')
@section('content')
<div class="container-fluid">
    <h3 class="text-dark mb-1" style="margin-top: 50px;">Gestion des types d'acte</h3>
</div>
<div class="table-responsive" style="background: #ffffff;margin-top: 25px;width: 950px;margin-left: 25px;">
    <table class="table">
        <thead>
            <tr>
                <th style="color: rgb(31,32,41);width: auto;">Type d'acte</th>
                <th style="color: rgb(31,32,41);width: auto;"></th>
                <th style="color: rgb(31,32,41);width: auto;"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <form action="ajoutertypeacte" method="post">
            @csrf
                <td><input type="text" name="typeacte" placeholder="Type d'acte" style="border-style: none;border-radius: 25px;background: rgb(255, 255, 255);"></td>
                <td><input type="text" name="code" placeholder="Code" style="border-style: none;border-radius: 25px;background: rgb(255, 255, 255);"></td>
                <td><input type="number" step="any" name="budgetannuel" placeholder="Budget annuel" style="border-style: none;border-radius: 25px;background: rgb(255, 255, 255);"></td>
                <td><button class="btn btn-primary" type="submit" style="background: #172a3a;border-style: none;">Ajouter</button></td>
                <td></td>
            </form>
            @if (isset($erreur))
            <p style="color: red">Echec d'insertion, le code utilisé existe déjà.</p>
            @endif
            </tr>
            @if($touslesactes!=null)
            @foreach($touslesactes as $act)
            @if($act->etat!=2)
            <tr>
                <form action="modifiertypeacte" method="post">
                    @csrf
                    <input type="hidden" name="id" value="{{$act->id}}">
                <td><input type="text" name="typeacte" value="{{$act->typeActe}}" style="border-style: none;border-radius: 25px;background: rgb(255, 255, 255);"></td>
                <td><input type="text" name="code" value="{{$act->code}}" style="border-style: none;border-radius: 25px;background: rgb(255, 255, 255);"></td>
                <td><input type="number" step="0.01" name="budgetannuel" value="{{$act->budgetAnnuel}}" style="border-style: none;border-radius: 25px;background: rgb(255, 255, 255);">Ar</td>
                <td><button class="btn btn-primary" type="submit" style="background: #7C7A7A;border-style: none;">Modifier</button></td>
            </form>
                <td>
                    <form action="supprimertypeacte/{{$act->id}}" method="post">
                    @csrf
                    <button class="btn btn-primary" type="submit" style="background: #551B14;border-style: none;">Supprimer</button>
                    </form>
                </td>
            </tr>
            @endif
            @endforeach
        @endif
        </tbody>
    </table>
</div>
@endsection
