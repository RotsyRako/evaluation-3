@extends('layouts.utilisateur')

@section('title', 'Depenses')
@section('content')

<div class="container-fluid">
    <h3 class="text-dark mb-1" style="margin-top: 50px;">Toutes les dépenses</h3>
</div>
<div class="table-responsive" style="background: #ffffff;margin-top: 25px;width: 700px;margin-left: 25px;">
    <table class="table">
        <thead>
            <tr>
                <th style="color: rgb(31,32,41);width: auto;">Intitulé</th>
                <th style="text-align: right;color: rgb(31,32,41);width: auto;">Cout</th>
                <th style="text-align: right;color: rgb(31,32,41);width: auto;">Date de dépense</th>
            </tr>
        </thead>
        <tbody>
        @if($depense!=null)
        @foreach($depense as $dep)
            <tr>
                <td><label class="form-label" style="border-color: rgb(133, 135, 150);border-top-color: rgb(133,;border-right-color: 135,;border-bottom-color: 150);border-left-color: 135,;">{{$dep->typeDepense}}</label></td>
                <td style="text-align: right;">{{number_format($dep->cout,2,'.',' ')}} Ar<br></td>
                <td style="text-align: right;">{{$dep->getDateFormatted()}}<br></td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
@endsection