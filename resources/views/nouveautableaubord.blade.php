@extends('layouts.admin')

@section('title', 'Tableau de bord')
@section('content')
<h3 class="text-dark mb-1" style="margin-top: 50px;">Tableau de bord</h3>
    <div style="margin-top: 20px;margin-bottom: 20px;">
        <form action="nouveauloadMonthData" method="post">
            @csrf
            <select style="margin-left: 10px;height: 30px;border-style: none;border-radius: 25px;"  name="mois">
                @for ($i = 0; $i < 12; $i++)
                <option value="{{$i+1}}">{{$monthNames[$i]}}</option>
                @endfor
            </select>
    <input type="number" name="annee" value="2023" style="margin-left: 10px;border-style: none;border-radius: 25px;height: 30px;">
    <button class="btn btn-primary" type="submit" style="margin-left: 10px;background: #172a3a;border-style: none;border-radius: 25px;">Afficher</button>
</form>
</div>

@if($condition==1)
<h4 style="color: #1B998B;">Recette</h4>
<div class="table-responsive" style="background: #ffffff; margin-bottom:20px ;margin-top: 25px;width: 950px;margin-left: 25px;">
    <table class="table">
        <thead>
            <tr>
                <th style="color: rgb(31,32,41);width: auto;">Type d'acte</th>
                <th style="text-align: right;color: rgb(31,32,41);width: auto;">Réel</th>
                <th style="text-align: right;color: rgb(31,32,41);width: auto;">Budget mensuel</th>
                <th style="text-align: right;color: rgb(31,32,41);width: auto;">Réalisation</th>
            </tr>
        </thead>
        <tbody>
        @foreach($typeacte as $t)
        <tr>
            <td>{{$t->typeActe}}</td>
            <td style="text-align: right;">{{$donneesrecettes[$t->id]->recettereelle}} Ar</td>
            <td style="text-align: right;">{{$donneesrecettes[$t->id]->recettebudgetmensuel}} Ar</td>
            <td style="text-align: right;">{{$donneesrecettes[$t->id]->realisationrecette}} %</td>
        </tr>
        @endforeach
        <tr>
            <td><strong>Total</strong></td>
            <td style="text-align: right;">{{$donneesbenef->sommerecettereelle}} Ar</td>
            <td style="text-align: right;">{{$donneesbenef->sommerecettebudgetmensuel}} Ar</td>
            <td style="text-align: right;">{{$donneesbenef->sommerecettebudrealisation}} %</td>
        </tr>
        </tbody>
    </table>
</div>

<h4 style="color: #9C0D38;">Dépenses</h4>    
<div class="table-responsive" style="margin-bottom: 25px;background: #ffffff;margin-top: 25px;width: 950px;margin-left: 25px;">
<table class="table">
        <thead>
            <tr>
                <th style="color: rgb(31,32,41);width: auto;">Type de dépense</th>
                <th style=" text-align: right;color: rgb(31,32,41);width: auto;">Réel</th>
                <th style=" text-align: right;color: rgb(31,32,41);width: auto;">Budget mensuel</th>
                <th style=" text-align: right;color: rgb(31,32,41);width: auto;">Réalisation</th>
            </tr>
        </thead>
        <tbody>
        @foreach($typedep as $t)
        <tr>
            <td>{{$t->typeDepense}}</td>
            <td style="text-align: right;">{{$donneesdepenses[$t->id]->depensereelle}} Ar</td>
            <td style="text-align: right;">{{$donneesdepenses[$t->id]->depensebudgetmensuel}} Ar</td>
            <td style="text-align: right;">{{$donneesdepenses[$t->id]->realisationdepense}} %</td>
        </tr>
        @endforeach
            <tr>
                <td><strong>Total</strong></td>
                <td style="text-align: right;">{{$donneesbenef->sommedepensereelle}} Ar</td>
                <td style="text-align: right;">{{$donneesbenef->sommedepensebudgetmensuel}} Ar</td>
                <td style="text-align: right;">{{$donneesbenef->sommedepensebudrealisation}} %</td>
            </tr>
        </tbody>
    </table>
</div>

<h4 style="color: #AB92BF;">Bénéfices</h4>    
<div class="table-responsive" style="background: #ffffff;margin-top: 25px;width: 950px;margin-left: 25px;">
<table class="table">
        <thead>
            <tr>
                <th style="color: rgb(31,32,41);width: auto;"></th>
                <th style="text-align: right;color: rgb(31,32,41);width: auto;">Réel</th>
                <th style="text-align: right;color: rgb(31,32,41);width: auto;">Budget mensuel</th>
                <th style="text-align: right;color: rgb(31,32,41);width: auto;">Réalisation</th>
            </tr>
        </thead>
        <tbody>
        <tr>
            <td>Recette</td>
            <td style="text-align: right;">{{$donneesbenef->sommerecettereelle}} Ar</td>
            <td style="text-align: right;">{{$donneesbenef->sommerecettebudgetmensuel}} Ar</td>
            <td style="text-align: right;">{{$donneesbenef->sommerecettebudrealisation}}%</td>
        </tr>
        <tr>
            <td>Dépense</td>
            <td style="text-align: right;">{{$donneesbenef->sommedepensereelle}} Ar</td>
            <td style="text-align: right;">{{$donneesbenef->sommedepensebudgetmensuel}} Ar</td>
            <td style="text-align: right;">{{$donneesbenef->sommedepensebudrealisation}}%</td>
        </tr>
        <tr>
            <td><strong>Total</strong></td>
            <td style="text-align: right;">{{$donneesbenef->beneficereel}} Ar</td>
            <td style="text-align: right;">{{$donneesbenef->beneficebudget}} Ar</td>
            <td style="text-align: right;">{{$donneesbenef->beneficerealisation}}%</td>
        </tr>
        </tbody>
    </table>
</div>

@endif
    
@endsection
