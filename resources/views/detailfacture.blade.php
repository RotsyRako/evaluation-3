@extends('layouts.utilisateur')

@section('title', 'Détails de la facture')
@section('content')
<div id="contenu">
        <div class="container-fluid">
            <h3 class="text-dark mb-1" style="margin-top: 50px;"><i class="fa fa-stethoscope" style="margin-right: 15px;"></i>Rotsy Clinique</h3>
        </div>
        <div class="table-responsive" style="background: #ffffff;margin-top: 25px;width: 850px;margin-left: 25px;">
            <table class="table">
                <thead>
                    <tr>
                        <th style="color: rgb(31,32,41);width: auto;">Informations utiles</th>
                        <th style="color: rgb(31,32,41);width: auto;"></th>
                        <th style="color: rgb(31,32,41);width: auto;"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="border-style: none;">
                        <label class="form-label" style="border-color: rgb(133, 135, 150);border-top-color: rgb(133,;border-right-color: 135,;border-bottom-color: 150);border-left-color: 135,;">Facture ID{{$facture->id}}
                        <br>Date : {{$datefacture}}
                        <br>Client : {{$facture->nom}}
                        <br>Né(e) le : {{$datenaissance}}
                        <br></label>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table-responsive" style="background: #ffffff;margin-top: 25px;width: 850px;margin-left: 25px;">
            <table class="table">
                <thead>
                    <tr>
                        <th style="color: rgb(31,32,41);width: auto;">Designation</th>
                        <th style="text-align: right; color: rgb(31,32,41);width: auto;">Prix</th>
                        <th style="color: rgb(31,32,41);width: auto;"></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($details as $d)
                    <tr>
                        <td><label class="form-label" style="border-color: rgb(133, 135, 150);border-top-color: rgb(133,;border-right-color: 135,;border-bottom-color: 150);border-left-color: 135,;">{{$d->typeActe}}</label></td>
                        <td style="text-align: right;">{{$d->prix}} Ar<br></td>
                        <td><br></td>
                    </tr>
                @endforeach
                    <tr>
                        <td><label class="form-label" style="border-color: rgb(133, 135, 150);border-top-color: rgb(133,;border-right-color: 135,;border-bottom-color: 150);border-left-color: 135,;color: #172a3a;font-weight: bold;">Total</label></td>
                        <td style="text-align: right; color: #172a3a;">{{$total}} Ar<br></td>
                        <td><br></td>
                    </tr>
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="table-responsive" style="background: #ffffff;margin-top: 25px;width: 850px;margin-left: 25px;">
        <table class="table">
            <thead>
                <tr></tr>
            </thead>
            <tbody style="border-style: none;">
                <tr>
                    <td style="border-style: none;background: var(--bs-table-bg);">
                    <label class="form-label" style="border-color: rgb(133, 135, 150);border-top-color: rgb(133,;border-right-color: 135,;border-bottom-color: 150);border-left-color: 135,;">
                    <button id="bouttonPDF" class="btn btn-primary" type="button" style="background: #172a3a;border-style: none;">Exporter le PDF</button>
                    </label></td>
                    <td><br></td>
                </tr>
            </tbody>
        </table>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.3.2/html2canvas.min.js"></script>
    <script>
        window.addEventListener('load', function() {
            var genererPDF=function(){
                var elementHTML = document.querySelector("#contenu");
                var doc = new jsPDF();
                var scale = 0.8;

            html2canvas(elementHTML, {
                scale: scale // Set the scale option
            }).then(function (canvas) {
                var imageData = canvas.toDataURL('image/png');
                doc.addImage(imageData, 'PNG', 10, 10, undefined, undefined);
                doc.save('Facture.pdf');
            });
        }
            document.getElementById("bouttonPDF").addEventListener("click",genererPDF);
        });
    </script>
@endsection